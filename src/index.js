import * as dbg from 'debug'
import chalk from 'chalk'
import clear from 'clear'
import figlet from 'figlet'
import minimist from 'minimist'
import configStore from 'configstore'
import * as inquirer from './lib/inquirer'
import * as files from './lib/files'
const pkg = require('../package.json');
import Gitlab from './lib/api'
const debug = dbg.debug('glab:index')

import ConfigStore from 'configstore'

import Repository from './lib/repo'

const conf = new ConfigStore(`${pkg.name}`)

if (!files.directoryExists('.git')) {
    console.log("gLab>", chalk.red('This is not a repository!'));
    process.exit(1);
}


clear()
console.log(
    chalk.yellow(
        figlet.textSync('gLab', { horizontalLayout: 'full' })
    )
);

const run = async () => {

    const api = new Gitlab({
        token: conf.get('gitlab.token'),
        configStore
    })

    if(!conf.get('gitlab.token')){
        const answer = await inquirer.askForGitlabToken()
        conf.set('gitlab.token', answer.token)
        debug('Token:', conf.get('gitlab.token'))

        try {
            const info = await api.user.info()
            conf.set('gitlab.user.id', info.id)
            conf.set('gitlab.user.name', info.name)
            conf.set('gitlab.user.username', info.username)
            conf.set('gitlab.user.email', info.email)
        } catch (e) {
            throw e
        }
    }

    const repo = new Repository(api)
    await repo.configure()
    let issues = (await repo.getIssues()).filter(i => i.state === 'opened')
    console.log(issues.map(i => ({
        id: i.id,
        iid: i.iid,
        title: i.title,
        description: i.description
    })))
    // const projects = await api.user.projects()
    // console.log(projects.length)
    // let git = new Repo('')
    // console.log(await repo.remotes)

    // console.log(projects
    //     .map(({
    //             name_with_namespace,
    //             ssh_url_to_repo,
    //             http_url_to_repo
    //         }) => ({name_with_namespace, ssh_url_to_repo, http_url_to_repo}))
    //     // .filter(rProj => rProj.ssh_url_to_repo ==)
    //     .sort())



}

run()