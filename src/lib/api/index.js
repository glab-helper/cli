import axios from 'axios'
import { Spinner } from 'clui'

import * as dbg from 'debug'
const debug = dbg.debug('glab:Gitlab')

import chalk from 'chalk'

var api = {
    token: '',
    url: 'https://gitlab.com/api/v4/'
}

const spinner = new Spinner('Waiting for Gitlab...')
var configStore = null

export default class Gitlab {
    constructor(opts){
        api.token = opts.token || null
        configStore = opts.configStore

        axios.interceptors.request.use((config) => {
            spinner.start()
            config.url = api.url + config.url
            config.headers['PRIVATE-TOKEN'] = api.token
            config.headers['content-type'] = 'application/json'
            config.headers['accept'] = 'application/json'
            return config
        }, err => {
            spinner.stop()
            return Promise.reject(err)
        })
        
        axios.interceptors.response.use((res) => {
            spinner.stop()
            return res
        }, err => {
            spinner.stop()
            debug(chalk.red("Failed request:", err.response.data.message))

            process.exit(err.response.status)
            return Promise.reject(err)
        })
    }

    get userId() {
        return configStore.get('gitlab.user.id')
    }

    request(method, uri, data = {}) {
        return new Promise((resolve, reject) => {
            axios[method.toLowerCase()](uri, data)
                .then(({
                    data
                }) => resolve(data))
                .catch(err => reject(err))
        })
    }

    get user() {
        return {
            info: () => this.request('get', '/user'),
            counts: () => this.request('get', "/user_counts"),
            status: () => this.request('get', "/user/status"),
            projects: () => this.request('get', '/projects?membership=true')
        }
    }

    get projects() {
        return {
            all: () => this.request('get', '/projects'),
            issues: id => this.request('get', `/projects/${id}/issues`)
        }
    }

}