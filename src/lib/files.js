import * as dbg from 'debug'
const debug = dbg.debug('glab:files')

import fs from 'fs'
import path from 'path'

export function getDirectoryBase() {
    return path.basename(process.cwd())
}

export function directoryExists (filePath) {
    debug(filePath)
    return fs.existsSync(filePath)
}