import * as dbg from 'debug'
const debug = dbg.debug('glab:inquirer')

import { prompt } from 'inquirer'
import * as files from './files'

export function askForGitlabToken() {
    const questions = [{
            name: 'token',
            type: 'input',
            message: 'Enter your GitLab API Token:',
            validate: function (value) {
                if (value.length) {
                    return true;
                } else {
                    return 'Please enter your API token.';
                }
            }
        }
    ];

    return prompt(questions);
}