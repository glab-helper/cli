import fs from 'fs'
import { Spinner } from 'clui'
import * as simplegit from 'simple-git/promise';
import * as files from './files'
import inquirer from './inquirer'

const git = simplegit.default()

var gitlab = null
var links = {}

export default class Repository {

    constructor(api){
        gitlab = api
    }
    
    configure(){
        return new Promise(async resolve => {
            await this.getGitlabRemote()
            resolve()
        })
    }
    
    getIssues(){
        return gitlab.projects.issues(this.gitlabId)
    }
    
    getRemotes() {
        return git.getRemotes(true)
    }

    getGitlabRemote() { return new Promise(async (resolve, reject) => {
        let localRemotes = (await this.getRemotes()).map(r => r.refs.push)
        gitlab.user.projects()
            .then((projects) => {
                let remote = projects.find(a => localRemotes.includes(a.ssh_url_to_repo) || localRemotes.includes(a.http_url_to_repo))
                this.gitlabId = remote.id
                links = remote._links
                resolve(remote.id)
            })
        })
    }
    

    getMergeRequests(id){
        
    }

    createRepository(){

    }
};